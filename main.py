import plotly
from pymongo import MongoClient
from pandas import DataFrame
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import cufflinks as cf
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from typing import List
import plotly.express as px
import plotly.graph_objects as go
import numpy as np
from sklearn.svm import SVR

def get_collection_from_database():
    # Provide the mongodb atlas url to connect python to mongodb using pymongo
    CONNECTION_STRING = "mongodb+srv://bigdata:bigdata@bigdata.bnose.mongodb.net/test"

    # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
    client = MongoClient(CONNECTION_STRING)

    # Create the database for our example (we will use the same database throughout the tutorial
    return client['bigdata']["HeartDiseases"]


def get_data():
    collection = get_collection_from_database()
    item_details = collection.find()
    # convert the dictionary objects to dataframe
    items_df = DataFrame(item_details)
    return items_df 


def general_health_by_age(data: DataFrame, graphicTitle: str, column: str):
    cant = len(data.index)
    data["GenHealthCount"] = 0
    data["ExcellentCount"] = 0
    data["FairCount"] = 0
    data["GoodCount"] = 0
    data["PoorCount"] = 0
    data["VeryGoodCount"] = 0

    # data_grouped = data.groupby(["AgeCategory", "GenHealth"]).agg({"ExcellentCount": "count",}).query("GenHealth == 'Excellent'")
    data_grouped = data.query("GenHealth == 'Excellent'").groupby(["AgeCategory"]).agg({"ExcellentCount": "count",})
    print(data_grouped[["ExcellentCount"]].values)

    # data_grouped = data.groupby(["AgeCategory", "GenHealth"])
    # data_grouped2 = data.groupby(["AgeCategory", "GenHealth"]).agg({"FairCount": "count",}).query("GenHealth == 'Fair'")
    data_grouped2 = data.query("GenHealth == 'Fair'").groupby(["AgeCategory"]).agg({"FairCount": "count",})
    data_grouped3 = data.query("GenHealth == 'Good'").groupby(["AgeCategory"]).agg({"GoodCount": "count",})
    data_grouped4 = data.query("GenHealth == 'Poor'").groupby(["AgeCategory"]).agg({"PoorCount": "count",})
    data_grouped5 = data.query("GenHealth == 'Very good'").groupby(["AgeCategory"]).agg({"VeryGoodCount": "count",})
    # data_grouped4 = data.groupby(["AgeCategory", "KidneyDisease"]).agg({"KidneyDiseaseCount": "count",}).query("KidneyDisease == 'Yes'")
    # data_grouped5 = data.groupby(["AgeCategory", "SkinCancer"]).agg({"SkinCancerCount": "count",}).query("SkinCancer == 'Yes'")
    # data_grouped2 = data.groupby(["AgeCategory", "Stroke"]).agg({"Stroke": "first", "Cantidad": "count",})
    # data_grouped3 = data.groupby(["AgeCategory", "Asthma"]).agg({"Asthma": "first", "Cantidad": "count",})

    # data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['HeartDiseaseCount']) * 100 / cant, axis=1)

    # df2 = pd.merge(data_grouped, data_grouped2, on=['AgeCategory'])
    df = pd.DataFrame()
    df["Excellent"] = data_grouped[["ExcellentCount"]]
    df["Fair"] = data_grouped2[["FairCount"]]
    df["Good"] = data_grouped3[["GoodCount"]]
    df["Poor"] = data_grouped4[["PoorCount"]]
    df["VeryGood"] = data_grouped5[["VeryGoodCount"]]

    # df1 = pd.concat([df, data_grouped["ExcellentCount"]], axis=1)

    # result = pd.concat([df1, data_grouped2["FairCount"]], axis=1)
    # result2 = pd.concat([result, data_grouped3["AsthmaCount"]], axis=1)
    # result3 = pd.concat([result2, data_grouped4["KidneyDiseaseCount"]], axis=1)
    # result4 = pd.concat([result3, data_grouped5["SkinCancerCount"]], axis=1)
    # print(result4.to_string())
    print(df.to_string())

    fig = px.imshow(df,
                title=graphicTitle,
                labels=dict(x="Estado general de salud", y="Rango de edad", color="GenHealthCount"),
                x=['Excellent', 'VeryGood', 'Good', 'Fair', 'Poor'],
                y=['18-24', '25-29', '30-34', '35-39', '40-44', '45-49', '50-54', '55-59', '60-64', '65-69', '70-74', '75-79', '80 or older']
               )
    fig.update_xaxes(side="top")
    fig.show()


def all_diseases_by_age_sex_smoking(data: DataFrame, graphicTitle: str, column: str):
    cant = len(data.index)
    data["AgeCategoryCount"] = 0
    data["SexCount"] = 0
    data["SmokingCount"] = 0

    data["HeartDiseaseCount"] = 0
    data["StrokeCount"] = 0
    data["AsthmaCount"] = 0
    data["KidneyDiseaseCount"] = 0
    data["SkinCancerCount"] = 0

    age_grouped = data.groupby(["AgeCategory"]).agg({"AgeCategory": "first",})
    data_grouped = data.groupby(["AgeCategory", "HeartDisease", "Sex", "Smoking"]).agg({"AgeCategoryCount": "first","SexCount": "first","SmokingCount": "first", "HeartDiseaseCount": "count",}).query("HeartDisease == 'Yes'")
    data_grouped2 = data.groupby(["AgeCategory", "Stroke", "Sex", "Smoking"]).agg({"StrokeCount": "count",}).query("Stroke == 'Yes'")
    data_grouped3 = data.groupby(["AgeCategory", "Asthma", "Sex", "Smoking"]).agg({"AsthmaCount": "count",}).query("Asthma == 'Yes'")
    data_grouped4 = data.groupby(["AgeCategory", "KidneyDisease", "Sex", "Smoking"]).agg({"KidneyDiseaseCount": "count",}).query("KidneyDisease == 'Yes'")
    data_grouped5 = data.groupby(["AgeCategory", "SkinCancer", "Sex", "Smoking"]).agg({"SkinCancerCount": "count",}).query("SkinCancer == 'Yes'")

    # data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['HeartDiseaseCount']) * 100 / cant, axis=1)

    # df2 = pd.merge(data_grouped, data_grouped2, on=['AgeCategory'])

    df = pd.DataFrame()
    # df["AgeCategory"] = data_grouped[["AgeCategoryCount", "SexCount", "SmokingCount"]]
    # df["Fair"] = data_grouped2[["FairCount"]]
    # df["Good"] = data_grouped3[["GoodCount"]]
    # df["Poor"] = data_grouped4[["PoorCount"]]
    # df["VeryGood"] = data_grouped5[["VeryGoodCount"]]

    df1 = pd.concat([df, data_grouped[["AgeCategoryCount", "SexCount", "SmokingCount"]]], axis=1)
    # result = pd.concat([df1, data_grouped2["StrokeCount"]], axis=1)
    # result2 = pd.concat([result, data_grouped3["AsthmaCount"]], axis=1)
    # result3 = pd.concat([result2, data_grouped4["KidneyDiseaseCount"]], axis=1)
    # result4 = pd.concat([result3, data_grouped5["SkinCancerCount"]], axis=1)

    # print(result4.to_string())
    print(df1.to_string())
    print(data_grouped.to_string())
    dftest = px.data.tips()
    print(dftest.to_string())

    

    fig1 = px.density_heatmap(data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="PhysicalActivity", title="Estado general de salud, por edad, sexo y realizacion de act. física")
    fig2 = px.density_heatmap(data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="HeartDisease")
    fig3 = px.density_heatmap(data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="SkinCancer")
    fig4 = px.density_heatmap(data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="KidneyDisease")
    fig5 = px.density_heatmap(data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="DiffWalking")
    fig6 = px.density_heatmap(data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="AlcoholDrinking")
    fig7 = px.density_heatmap(data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="Smoking")
    f1 = px.density_heatmap(data, x="Race", y="AgeCategory", z="PhysicalHealth" , histfunc="avg", facet_row="Sex", facet_col="PhysicalActivity")
    f2 = px.density_heatmap(data, x="Race", y="AgeCategory", z="MentalHealth" , histfunc="avg", facet_row="Sex", facet_col="PhysicalActivity")

    fig = px.scatter_ternary(data, a="PhysicalHealth", b="MentalHealth", c="SleepTime", hover_name="Sex",
        color="PhysicalActivity" )
    fig.show()




    # GRAFICO 3D

    # mesh_size = .02
    # margin = 0

    # X = data[['MentalHealth', 'PhysicalHealth']]
    # y = data['SleepTime']

    # # Condition the model on sepal width and length, predict the petal width
    # model = SVR(C=1.)
    # model.fit(X, y)

    # # Create a mesh grid on which we will run our model
    # x_min, x_max = X.MentalHealth.min() - margin, X.MentalHealth.max() + margin
    # y_min, y_max = X.PhysicalHealth.min() - margin, X.PhysicalHealth.max() + margin
    # xrange = np.arange(x_min, x_max, mesh_size)
    # yrange = np.arange(y_min, y_max, mesh_size)
    # xx, yy = np.meshgrid(xrange, yrange)

    # # Run model
    # pred = model.predict(np.c_[xx.ravel(), yy.ravel()])
    # pred = pred.reshape(xx.shape)

    # # Generate the plot
    # fig = px.scatter_3d(df, x='MentalHealth', y='PhysicalHealth', z='SleepTime')
    # fig.update_traces(marker=dict(size=5))
    # fig.add_traces(go.Surface(x=xrange, y=yrange, z=pred, name='pred_surface'))
    # fig.show()


    f1.show()
    f2.show()
    fig1.show()
    fig2.show()
    fig3.show()
    fig4.show()
    fig5.show()
    fig6.show()
    fig7.show()


def all_diseases_by_age(data: DataFrame, graphicTitle: str, column: str):
    cant = len(data.index)
    data["HeartDiseaseCount"] = 0
    data["StrokeCount"] = 0
    data["AsthmaCount"] = 0
    data["KidneyDiseaseCount"] = 0
    data["SkinCancerCount"] = 0

    # data_grouped = data.groupby(["HeartDisease", "Stroke", "Diabetic", "Asthma", "KidneyDisease", "SkinCancer" ]).agg({"HeartDisease": "first", "Stroke": "first", "Diabetic": "first", "Asthma": "first", "KidneyDisease": "first", "SkinCancer": "first", "Cantidad": "count",})

    data_grouped = data.groupby(["AgeCategory", "HeartDisease"]).agg({"HeartDiseaseCount": "count",}).query("HeartDisease == 'Yes'")
    data_grouped2 = data.groupby(["AgeCategory", "Stroke"]).agg({"StrokeCount": "count",}).query("Stroke == 'Yes'")
    data_grouped3 = data.groupby(["AgeCategory", "Asthma"]).agg({"AsthmaCount": "count",}).query("Asthma == 'Yes'")
    data_grouped4 = data.groupby(["AgeCategory", "KidneyDisease"]).agg({"KidneyDiseaseCount": "count",}).query("KidneyDisease == 'Yes'")
    # data_grouped5 = data.groupby(["AgeCategory", "SkinCancer"]).agg({"SkinCancerCount": "count",}).query("SkinCancer == 'Yes'")
    # data_grouped2 = data.groupby(["AgeCategory", "Stroke"]).agg({"Stroke": "first", "Cantidad": "count",})
    # data_grouped3 = data.groupby(["AgeCategory", "Asthma"]).agg({"Asthma": "first", "Cantidad": "count",})

    # data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['HeartDiseaseCount']) * 100 / cant, axis=1)

    # df2 = pd.merge(data_grouped, data_grouped2, on=['AgeCategory'])
    df = pd.DataFrame()

    df1 = pd.concat([df, data_grouped["HeartDiseaseCount"]], axis=1)
    result = pd.concat([df1, data_grouped2["StrokeCount"]], axis=1)
    result2 = pd.concat([result, data_grouped3["AsthmaCount"]], axis=1)
    result3 = pd.concat([result2, data_grouped4["KidneyDiseaseCount"]], axis=1)
    # result4 = pd.concat([result3, data_grouped5["SkinCancerCount"]], axis=1)
    print("Afecciones x edad, haciendo gim")
    print(result3.to_string())
    print(data_grouped.to_string())

    fig = px.imshow(result3,
                title=graphicTitle,
                labels=dict(x="Enfermedades", y="Rango de edad", color="HeartDiseaseCount"),
                x=['HeartDiseaseCount', 'StrokeCount', 'AsthmaCount', 'KidneyDiseaseCount'],
                y=['18-24', '25-29', '30-34', '35-39', '40-44', '45-49', '50-54', '55-59', '60-64', '65-69', '70-74', '75-79', '80 or older']
               )
    fig.update_xaxes(side="top")
    fig.show()

    # fig2 = px.density_heatmap(df, x="total_bill", y="tip", facet_row="Sex", facet_col="Smoking")
    # fig2.show()


def all_diseases(data: DataFrame, graphicTitle: str, column: str):
    cant = len(data.index)
    data["Cantidad"] = 0

    # data_grouped = data.groupby(["HeartDisease", "Stroke", "Diabetic", "Asthma", "KidneyDisease", "SkinCancer" ]).agg({"HeartDisease": "first", "Stroke": "first", "Diabetic": "first", "Asthma": "first", "KidneyDisease": "first", "SkinCancer": "first", "Cantidad": "count",})

    data_grouped = data.groupby(["HeartDisease", "Stroke", "Diabetic", "Asthma", "KidneyDisease", "SkinCancer" ]).agg({"HeartDisease": "first", "Stroke": "first", "Diabetic": "first", "Asthma": "first", "KidneyDisease": "first", "SkinCancer": "first", "Cantidad": "count",})


    data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['Cantidad']) * 100 / cant, axis=1)
    
    print(data_grouped.to_string())

    # fig = px.bar(data, x=["HeartDisease", "Stroke", "Diabetic", "Asthma", "KidneyDisease", "SkinCancer"], y=["Yes", "No"], title="Wide-Form Input")
    # fig.show()




def other_diseases(data: DataFrame, graphicTitle: str, column: str):
    cant = len(data.index)
    data["Cantidad"] = 0

    data_grouped = data.groupby(["" + column + ""]).agg({"" + column + "": "first", "Cantidad": "count",})
    # data_grouped = data.groupby(["AgeCategory"])["Cantidad"].agg("count").reset_index()
    # data_grouped["PhysicalHealth"].agg("first")
    # data_grouped = data.groupby(["AgeCategory", 'PhysicalHealth', 'MentalHealth']).count().reset_index()

    data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['Cantidad']) * 100 / cant, axis=1)
    
    print(data_grouped)

    fig = px.pie(data_grouped, values='Cantidad', names="" + column + "", title=graphicTitle, color="" + column + "")
    # fig = go.Pie(labels='Sex', values='Cantidad', scalegroup='one',
    #                 name=graphicTitle)

    # fig.add_trace(go.Pie(labels='Sex', values='Cantidad', scalegroup='one',
    #                  name="World GDP 1980"), 1, 1)

    fig.update_traces(text=data_grouped['Cantidad'], textinfo='percent+label+text', textfont_size=16)
    fig.show()


def smoking_drinking(data: DataFrame, graphicTitle: str):
    cant = len(data.index)
    data["Cantidad"] = 0

    data_grouped = data.groupby(["AgeCategory", "AlcoholDrinking", "Smoking"]).agg({"AgeCategory": "first", "AlcoholDrinking": "first", "Smoking": "first", "Cantidad": "count",})
    # data_grouped = data.groupby(["AgeCategory"])["Cantidad"].agg("count").reset_index()
    # data_grouped["PhysicalHealth"].agg("first")
    # data_grouped = data.groupby(["AgeCategory", 'PhysicalHealth', 'MentalHealth']).count().reset_index()

    data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['Cantidad']) * 100 / cant, axis=1)
    
    print(data_grouped)

    fig = px.scatter(data_grouped, x="AlcoholDrinking", y="Smoking",
	         size="Cantidad", color="AgeCategory",
                 hover_name="AgeCategory")
    fig.show()


def mental_physical_by_age(data: DataFrame, graphicTitle: str):
    cant = len(data.index)
    data["Cantidad"] = 0

    data_grouped = data.groupby(["AgeCategory", "PhysicalActivity"]).agg({"AgeCategory": "first", "PhysicalActivity": "first", "PhysicalHealth": "mean", "MentalHealth": "mean", "SleepTime": "mean", "Cantidad": "count",})


    # data_grouped = data.groupby(["AgeCategory"])["Cantidad"].agg("count").reset_index()
    # data_grouped["PhysicalHealth"].agg("first")
    # data_grouped = data.groupby(["AgeCategory", 'PhysicalHealth', 'MentalHealth']).count().reset_index()

    # data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['Cantidad']) * 100 / cant, axis=1)
    
    print("Mental and Physical health")
    print(data_grouped)

    fig = px.bar(data_grouped, x="AgeCategory", y="PhysicalHealth",
             color="PhysicalActivity", hover_data=["PhysicalHealth", "MentalHealth", "Cantidad"],
             barmode = 'group', title="Salud física: " + graphicTitle)

    fig2 = px.bar(data_grouped, x="AgeCategory", y="MentalHealth",
             color="PhysicalActivity", hover_data=["PhysicalHealth", "MentalHealth", "Cantidad"],
             barmode = 'group', title="Salud mental: " + graphicTitle)
   
    fig.show()
    fig2.show()

    # fig = px.scatter(data_grouped, x="PhysicalHealth", y="MentalHealth",
	#          size="Cantidad", color="AgeCategory",
    #              hover_name="AgeCategory", log_x=True, size_max=60)
    # fig.show()


def group_by_heart_disease(data: DataFrame, graphicTitle: str):
    cant = len(data.index)
    data["Cantidad"] = 0
    data_grouped = data.groupby(["HeartDisease"])["Cantidad"].agg("count").reset_index()
    data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['Cantidad']) * 100 / cant, axis=1)
    print(data_grouped)
    fig = px.pie(data_grouped, values='Cantidad', names='HeartDisease', title=graphicTitle)
    # fig = go.Pie(labels=data_grouped['AgeCategory'], values=data_grouped['Cantidad'], scalegroup='one',
    #                 name=graphicTitle)
    fig.update_traces(text=data_grouped['Cantidad'], textinfo='percent+label+text', textfont_size=16)
    fig.show()


def group_by_gender(data: DataFrame, graphicTitle: str):
    cant = len(data.index)
    data["Cantidad"] = 0

    # data_grouped = data.groupby(["Sex", "PhysicalActivity"]).agg({"Sex": "first", "PhysicalActivity": "first", "PhysicalHealth": "mean", "MentalHealth": "mean", "SleepTime": "mean", "Cantidad": "count",})
    # data_grouped = data.groupby(["Sex", "AlcoholDrinking"]).agg({"Sex": "first", "AlcoholDrinking": "first", "PhysicalHealth": "mean", "MentalHealth": "mean", "SleepTime": "mean", "Cantidad": "count",})
    data_grouped = data.groupby(["Sex", "Smoking"]).agg({"Sex": "first", "Smoking": "first", "PhysicalHealth": "mean", "MentalHealth": "mean", "SleepTime": "mean", "Cantidad": "count",})
    
    
    # data_grouped = data.groupby(["Sex"])["Cantidad"].agg("count").reset_index()
    data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['Cantidad']) * 100 / cant, axis=1)

    print(data_grouped)

   

    fig = px.bar(data_grouped, x="Sex", y="Cantidad",
             color="Smoking", hover_data=["PhysicalHealth", "MentalHealth", "Cantidad"],
             barmode = 'group', title="Salud física: " + graphicTitle)

    # fig2 = px.bar(data_grouped, x="Sex", y="MentalHealth",
    #          color="Smoking", hover_data=["PhysicalHealth", "MentalHealth", "Cantidad"],
    #          barmode = 'group', title="Salud mental: " + graphicTitle)
   
    fig.show()

    fig3 = px.histogram(data_grouped, x="Sex", y="Cantidad", color="Sex", pattern_shape="Smoking")
    fig3.show()

    # fig2.show()

    # fig = px.pie(data_grouped, values='Cantidad', names='Sex', title=graphicTitle, color="Sex", color_discrete_map={'Male':'royalblue',
    #                              'Female':'cyan'})
    # fig = go.Pie(labels='Sex', values='Cantidad', scalegroup='one',
    #                 name=graphicTitle)

    # fig.add_trace(go.Pie(labels='Sex', values='Cantidad', scalegroup='one',
    #                  name="World GDP 1980"), 1, 1)

    # fig.update_traces(text=data_grouped['Cantidad'], textinfo='percent+label+text', textfont_size=16)
    # fig.show()


def group_by_age(data: DataFrame, graphicTitle: str):
    cant = len(data.index)
    data["Cantidad"] = 0
    data_grouped = data.groupby(["AgeCategory"])["Cantidad"].agg("count").reset_index()
    data_grouped["Porcentaje"] = data_grouped.apply(lambda x: int(x['Cantidad']) * 100 / cant, axis=1)
    print(data_grouped)
    fig = px.pie(data_grouped, values='Cantidad', names='AgeCategory', title=graphicTitle)
    # fig = go.Pie(labels=data_grouped['AgeCategory'], values=data_grouped['Cantidad'], scalegroup='one',
    #                 name=graphicTitle)
    fig.update_traces(text=data_grouped['Cantidad'], textinfo='percent+label+text', textfont_size=16)
    fig.show()



def mental_health(data: DataFrame, columns: List[str]):
    # data_grouped_mean = data.groupby(columns).agg({'MentalHealth': "mean"})
    # data_grouped_count = data.groupby(columns).agg({"AgeCategory": "count"})

    data_grouped = data.groupby(columns).agg({'PhysicalHealth': "mean", 'MentalHealth': "mean", "SleepTime": "count"})
    print(data_grouped)
    # df = px.data.iris()
    # fig = px.scatter(data_grouped, x="MentalHealth", y='AgeCategory', size="SleepTime")

    #fig = px.scatter(data_grouped, x="MentalHealth", y="PhysicalHealth", size="SleepTime")
    #fig.show()

    # data_grouped = data.groupby(columns).filter(lambda x: any(x['AgeCategory'] == "'18 - 24'")).agg({'PhysicalHealth': "mean", 'MentalHealth': "mean"})
    # data_grouped = data.groupby(columns).apply(lambda x: any(x['AgeCatclearegory'] == "18 - 24")).agg({'PhysicalHealth': "mean", 'MentalHealth': "mean"})
    
    # data_grouped = data_grouped.where("AgeCategory == '18 - 24'")

    # data_grouped.iplot()
    # data_grouped.iplot(kind="bar", xTitle="Rango de edad", yTitle="Salud mental y física", title="holis")
    # data_grouped.iplot(kind="box")

    # data_grouped_mean.plot(kind="barh")
    # data_grouped_count.plot(kind="barh")
    # print(data_grouped)
    # print(data_grouped_mean)
    # print(data_grouped_count)
    # return data_grouped


def physical_health(data: DataFrame, columns: List[str]):
    data_grouped_mean = data.groupby(columns).agg({'PhysicalHealth': "mean"})
    data_grouped_count = data.groupby(columns).agg({"AgeCategory": "count"})
    # data_grouped_mean.plot(kind="barh")
    # data_grouped_count.plot(kind="barh")
    print(data_grouped_mean)
    print(data_grouped_count)


def relations(data: DataFrame):
    new_data = data[['PhysicalHealth', 'MentalHealth', 'SleepTime']]
    sns.set(style="ticks")
    # sns.pairplot(new_data, hue="PhysicalHealth", palette="Spectral")
    sns.lmplot(data=data, x="SleepTime", y="MentalHealth")



def big_data_app():
    setattr(plotly.offline, "__PLOTLY_OFFLINE_INITIALIZED", True)

    cf.set_config_file(sharing='public', theme='ggplot', offline=True)
    cf.go_offline()

    df = pd.read_csv('heart_2020_cleaned.csv')
    # data = get_data()

    only_physical_activity_data = df.query("PhysicalActivity == 'Yes'")
    only_no_physical_activity_data = df.query("PhysicalActivity == 'No'")

    group_by_heart_disease(only_physical_activity_data, "Cantidad de personas que tuvieron o no enfermedad, y ejercitaron los ultimos 30 dias")
    group_by_heart_disease(only_no_physical_activity_data, "Cantidad de personas que tuvieron o no enfermedad, y no ejercitaron los ultimos 30 dias")

    only_heart_diseases_data = df.query("HeartDisease == 'Yes'")
    only_no_heart_diseases_data = df.query("HeartDisease == 'No'")

    # only_heart_diseases_data.pivot(index="GenHealth", columns=["AgeCategory"],values=["GenHealth"])
    # print(only_heart_diseases_data)

    group_by_age(only_heart_diseases_data, "Personas que tuvieron una enfermedad cardíaca, agrupadas por edad")
    group_by_age(only_no_heart_diseases_data, "Personas que no tuvieron una enfermedad cardíaca, agrupadas por edad")


    group_by_gender(only_heart_diseases_data, "Personas que tuvieron una enfermedad cardíaca, agrupadas por sexo")
    group_by_gender(only_no_heart_diseases_data, "Personas que no tuvieron una enfermedad cardíaca, agrupadas por sexo")


    mental_physical_by_age(only_heart_diseases_data, "personas que tuvieron una enfermedad cardíaca")
    mental_physical_by_age(only_no_heart_diseases_data, "personas que no tuvieron una enfermedad cardíaca")

    smoking_drinking(only_heart_diseases_data, "")

    other_diseases(df, "Enfermedad cardiaca", "HeartDisease")
    other_diseases(df, "Cáncer de piel", "SkinCancer")
    other_diseases(df, "Enfermedad renal", "KidneyDisease")
    other_diseases(df, "Asma", "Asthma")
    other_diseases(df, "Diabetes", "Diabetic")
    other_diseases(df, "Derrame cerebral", "Stroke")

    all_diseases(df, "", "")

    all_diseases_by_age(only_physical_activity_data, "Afecciones por edad, realizando act. física", "")

    all_diseases_by_age(only_no_physical_activity_data, "Afecciones por edad, no realizando act. física", "")

    general_health_by_age(only_physical_activity_data, "Percepcion personal de salud en general, realizando act. fisica", "")
    general_health_by_age(only_no_physical_activity_data, "Percepcion personal de salud en general, no realizando act. fisica", "")

    all_diseases_by_age_sex_smoking(df,"","")
    f1 = px.density_heatmap(only_heart_diseases_data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="Smoking")
    f2 = px.density_heatmap(only_no_heart_diseases_data, x="GenHealth", y="AgeCategory", facet_row="Sex", facet_col="Smoking")
    f1.show()
    f2.show()
    #all_diseases_by_age_sex_smoking(only_heart_diseases_data, "Personas con enfermedad cardiaca", "")
    #all_diseases_by_age_sex_smoking(only_no_heart_diseases_data, "Personas sin enfermedad cardiaca", "")
    # mental_health(only_heart_diseases_data, ['Race', 'Sex'])

    # mental_health(only_heart_diseases_data, ['AgeCategory', 'AlcoholDrinking', 'PhysicalActivity'])
    # mental_health(only_no_heart_diseases_data, ['AgeCategory', 'AlcoholDrinking', 'PhysicalActivity'])
    # physical_health(only_heart_diseases_data, ['AgeCategory'])
    # relations(only_heart_diseases_data)
    plt.show()


def app():
    data = get_data()

    # WITH HEART DISEASE
    print("- Heart Disease -")
    only_heart_diseases = data.query("HeartDisease == 'Yes'")

    sleep_by_age_heart_disease = only_heart_diseases.groupby(['AgeCategory', 'SleepTime']).agg({"AgeCategory": "count"})

    bmi_heart_disease = only_heart_diseases.groupby('AgeCategory').agg(
        {"AgeCategory": "count", "BMI": "max"
         })
    whoSmokedWithHeartDisease = only_heart_diseases.groupby('Smoking').agg({"Smoking": "count", "SleepTime": "mean"})
    sleeptime_with_heart_disease = only_heart_diseases.groupby('SleepTime').agg({"SleepTime": "count"})
    who_drinks_with_heart_disease = only_heart_diseases.groupby('AlcoholDrinking').agg({"AlcoholDrinking": "count"})
    gen_health_with_heart_disease = only_heart_diseases.groupby('GenHealth').agg({"GenHealth": "count"})
    age_with_heart_disease = only_heart_diseases.groupby('AgeCategory').agg({"AgeCategory": "count"})

    whoSmokedWithHeartDisease.plot.bar(rot=0)
    who_drinks_with_heart_disease.plot.bar(rot=0)
    gen_health_with_heart_disease.plot.bar(rot=0)
    age_with_heart_disease.plot.bar(rot=0)
    bmi_heart_disease.plot.bar(rot=0)

    print(whoSmokedWithHeartDisease)
    # print(who_drinks_with_heart_disease)
    # print(gen_health_with_heart_disease)
    # print(sleeptime_with_heart_disease)
    # print(age_with_heart_disease)
    print(sleep_by_age_heart_disease.to_string())
    print(bmi_heart_disease.to_string())

    howMuchSleepedHeartDiseases = only_heart_diseases.groupby('SleepTime').agg({"SleepTime": "count"})

    howMuchSleepedHeartDiseases.plot(kind="bar")

    print("---------------")
    # WITH NO HEART DISEASE
    print("- No Heart Disease- ")

    only_no_heart_diseases = data.query("HeartDisease == 'No'")

    whoSmokedWithNoHeartDisease = only_no_heart_diseases.groupby('Smoking').agg(
        {"Smoking": "count", "SleepTime": "mean"}
    )
    howMuchSleepedNoHeartDiseases = only_no_heart_diseases.groupby('SleepTime').agg({"SleepTime": "count"})
    who_drinks_with_no_heart_disease = only_no_heart_diseases.groupby('AlcoholDrinking').agg(
        {"AlcoholDrinking": "count"}
    )
    gen_health_with_no_heart_disease = only_no_heart_diseases.groupby('GenHealth').agg({"GenHealth": "count"})
    sleeptime_with_no_heart_disease = only_no_heart_diseases.groupby('SleepTime').agg({"SleepTime": "count"})
    age_with_no_heart_disease = only_no_heart_diseases.groupby('AgeCategory').agg({"AgeCategory": "count"})

    bmi_no_heart_disease = only_no_heart_diseases.groupby('AgeCategory').agg(
        {"AgeCategory": "count", "BMI": "max"
         })

    howMuchSleepedNoHeartDiseases.plot(kind="bar")
    gen_health_with_no_heart_disease.plot(kind="bar")
    age_with_no_heart_disease.plot(kind="bar")
    bmi_no_heart_disease.plot(kind="bar")

    print(whoSmokedWithNoHeartDisease)
    # print(who_drinks_with_no_heart_disease)
    # print(gen_health_with_no_heart_disease)
    # print(sleeptime_with_no_heart_disease)
    # print(age_with_no_heart_disease)
    print(bmi_no_heart_disease)

    plt.show()


if __name__ == '__main__':
    big_data_app()
